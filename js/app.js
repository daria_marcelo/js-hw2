// 1) number, string, boolean, undefined, BigInt, null, object, symbol
// 2) == нестрога рівність (приводить до одного типу), а === строга рівність (100% ідентичність)
// 3)  внутрішня функція JS, яка виконує певну дію і повертає результат ( + - *  і тд)

function askUserInfo() {
    let name = prompt('What is your name?');
    if (name === null) {
        return alert('You are not allowed to visit this website.');
    }

    while (name === '') {
        name = prompt('Enter your name!');

        if (name === null) {
            return alert('You are not allowed to visit this website.');
        }
    }

    let age = prompt('How old are you?');
    if (age === null) {
        return alert('You are not allowed to visit this website.');
    }

    while (age === '' || isNaN(+age)) {
        age = prompt('Enter your age!');
        if (age === null) {
            return alert('You are not allowed to visit this website.');
        }
    }

    age = +age;

    if (age < 18) {
        alert('You are not allowed to visit this website.');
    } else if (age > 22) {
        alert(`Welcome, ${name}!`);
    } else if (18 <= age <= 22) {
        if (confirm('Are you sure you want to continue?')) {
            alert(`Welcome, ${name}!`);
        } else {
            alert('You are not allowed to visit this website.');
        }
    }
}
askUserInfo();